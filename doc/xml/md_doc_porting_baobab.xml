<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="compound.xsd" version="1.9.1" xml:lang="en-US">
  <compounddef id="md_doc_porting_baobab" kind="page">
    <compoundname>md_doc_porting_baobab</compoundname>
    <title>baobab</title>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
<para>OGDF » <ref refid="md_doc_porting" kindref="compound">Porting Guide</ref> » Baobab</para>
<sect1 id="md_doc_porting_baobab_1autotoc_md12">
<title>Porting from Sakura to Baobab</title>
<sect2 id="md_doc_porting_baobab_1autotoc_md13">
<title>Classes</title>
<sect3 id="md_doc_porting_baobab_1autotoc_md14">
<title>List, SList, ListPure, SListPure search() method</title>
<para>The <computeroutput>search(element)</computeroutput> method returned a position index. It now returns a <computeroutput>ListIterator</computeroutput> or <computeroutput>ListConstIterator</computeroutput>.</para>
<para><table rows="4" cols="3"><row>
<entry thead="yes"><para>use case   </para>
</entry><entry thead="yes"><para>old code   </para>
</entry><entry thead="yes"><para>new code    </para>
</entry></row>
<row>
<entry thead="no"><para>check if element is a member of the list   </para>
</entry><entry thead="no"><para><computeroutput>list.search(element) != -1</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>list.search(element).valid()</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para>find the iterator for an element   </para>
</entry><entry thead="no"><para><computeroutput>list.get(list.search(element))</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>list.search(element)</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para>find the position index of an element   </para>
</entry><entry thead="no"><para><computeroutput>list.search(element)</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>list.pos(list.search(element))</computeroutput>   </para>
</entry></row>
</table>
</para>
</sect3>
<sect3 id="md_doc_porting_baobab_1autotoc_md15">
<title>ClusterGraph</title>
<para>Some methods for reading and writing graphs have been <bold>moved</bold> to <computeroutput>GraphIO</computeroutput> (as static methods, where the <computeroutput>ClusterGraph</computeroutput> is the first parameter):</para>
<para><table rows="4" cols="2"><row>
<entry thead="yes"><para>old method   </para>
</entry><entry thead="yes"><para>new method    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>readClusterGML</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>GraphIO::readGML</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>writeGML</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>GraphIO::writeGML</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>readClusterGraphOGML</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>GraphIO::readOGML</computeroutput>   </para>
</entry></row>
</table>
</para>
</sect3>
<sect3 id="md_doc_porting_baobab_1autotoc_md16">
<title>ClusterGraphAttributes</title>
<para>Some <bold>attribute access methods</bold> have been renamed for consistency. The following table shows the changes.</para>
<para><table rows="14" cols="3"><row>
<entry thead="yes"><para>old name   </para>
</entry><entry thead="yes"><para>new name   </para>
</entry><entry thead="yes"><para>remark    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>clusterBackColor</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>fillBgColor</computeroutput>   </para>
</entry><entry thead="no"><para>colors are now represented by <computeroutput>Color</computeroutput> (instead of <computeroutput>String</computeroutput>)    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>clusterColor</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>strokeColor</computeroutput>   </para>
</entry><entry thead="no"><para>colors are now represented by <computeroutput>Color</computeroutput> (instead of <computeroutput>String</computeroutput>)    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>clusterFillColor</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>fillColor</computeroutput>   </para>
</entry><entry thead="no"><para>colors are now represented by <computeroutput>Color</computeroutput> (instead of <computeroutput>String</computeroutput>)    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>clusterFillPattern</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>fillPattern</computeroutput>   </para>
</entry><entry thead="no"><para>attribute type has been changed from <computeroutput>GraphAttributes::BrushPattern</computeroutput> to <computeroutput>FillPattern</computeroutput>; use <computeroutput>setFillPattern</computeroutput> to change the attribute value    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>clusterHeight</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>height</computeroutput>   </para>
</entry><entry thead="no"><para></para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>clusterLabel</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>label</computeroutput>   </para>
</entry><entry thead="no"><para></para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>clusterLineStyle</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>strokeType</computeroutput>   </para>
</entry><entry thead="no"><para>attribute type has been changed from <computeroutput>GraphAttributes::EdgeStyle</computeroutput> to <computeroutput>StrokeType</computeroutput>; use <computeroutput>setStrokeType</computeroutput> to change the attribute value    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>clusterLineWidth</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>strokeWidth</computeroutput>   </para>
</entry><entry thead="no"><para>attribute type has been changed from <computeroutput>double</computeroutput> to <computeroutput>float</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>clusterWidth</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>width</computeroutput>   </para>
</entry><entry thead="no"><para></para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>clusterXPos</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>x</computeroutput>   </para>
</entry><entry thead="no"><para></para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>clusterYPos</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>y</computeroutput>   </para>
</entry><entry thead="no"><para></para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>setClusterFillPattern</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>setFillPattern</computeroutput>   </para>
</entry><entry thead="no"><para>attribute type has been changed from <computeroutput>GraphAttributes::BrushPattern</computeroutput> to <computeroutput>FillPattern</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>setClusterLineStyle</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>setStrokeType</computeroutput>   </para>
</entry><entry thead="no"><para>attribute type has been changed from <computeroutput>GraphAttributes::EdgeStyle</computeroutput> to <computeroutput>StrokeType</computeroutput>   </para>
</entry></row>
</table>
</para>
<para>Access to cluster attributes by <bold>cluster index</bold> has been removed. Use index by cluster instead.</para>
<para>The method <computeroutput>clusterID(node v)</computeroutput> has been <bold>removed</bold>. Use <computeroutput>cga.clusterOf(v)-&gt;index()</computeroutput> instead (if <computeroutput>cga</computeroutput> is an instance of <computeroutput>ClusterGraphAttributes</computeroutput>).</para>
<para>Some methods for reading and writing graphs have been <bold>moved</bold> to <computeroutput>GraphIO</computeroutput> (as static methods, where <computeroutput>ClusterGraphAttributes</computeroutput> is the first parameter):</para>
<para><table rows="5" cols="2"><row>
<entry thead="yes"><para>old method   </para>
</entry><entry thead="yes"><para>new method    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>readClusterGML</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>GraphIO::readGML</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>writeGML</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>GraphIO::writeGML</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>readClusterGraphOGML</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>GraphIO::readOGML</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>writeOGML</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>GraphIO::writeOGML</computeroutput>   </para>
</entry></row>
</table>
</para>
</sect3>
<sect3 id="md_doc_porting_baobab_1autotoc_md17">
<title>DisjointSets</title>
<para>DisjointSets manages disjoint sets of <emphasis>integers</emphasis> (set IDs) from zero to the maximum number of sets. The mapping (for example from nodes to set IDs) is the responsibility of the user. The first template parameter (the type of the set elements) is now gone because it was not necessary.</para>
<para>The code <programlisting><codeline><highlight class="normal"><sp/>{c++}</highlight></codeline>
<codeline><highlight class="normal">include<sp/>&lt;ogdf/basic/DisjointSets.h&gt;</highlight></codeline>
<codeline><highlight class="normal">…</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>DisjointSets&lt;edge&gt;<sp/>uf(G.numberOfEdges());</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>EdgeArray&lt;int&gt;<sp/>setID(G);</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>…</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>edge<sp/>e;</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>…</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>setID[e]<sp/>=<sp/>uf.makeSet(e);</highlight></codeline>
<codeline><highlight class="normal">…</highlight></codeline>
</programlisting> simply becomes <programlisting><codeline><highlight class="normal"><sp/>{c++}</highlight></codeline>
<codeline><highlight class="normal">include<sp/>&lt;ogdf/basic/DisjointSets.h&gt;</highlight></codeline>
<codeline><highlight class="normal">…</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>DisjointSets&lt;&gt;<sp/>uf(G.numberOfEdges());</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>EdgeArray&lt;int&gt;<sp/>setID(G);</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>…</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>edge<sp/>e;</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>…</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>setID[e]<sp/>=<sp/>uf.makeSet();</highlight></codeline>
<codeline><highlight class="normal">…</highlight></codeline>
</programlisting></para>
</sect3>
<sect3 id="md_doc_porting_baobab_1autotoc_md18">
<title>Graph</title>
<para>Some methods for reading and writing graphs have been <bold>moved</bold> to <computeroutput>GraphIO</computeroutput> (as static methods, where the <computeroutput>Graph</computeroutput> is the first parameter):</para>
<para><table rows="4" cols="2"><row>
<entry thead="yes"><para>old method   </para>
</entry><entry thead="yes"><para>new method    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>readGML</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>GraphIO::readGML</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>writeGML</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>GraphIO::writeGML</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>readLEDAGraph</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>GraphIO::readLEDA</computeroutput>   </para>
</entry></row>
</table>
</para>
</sect3>
<sect3 id="md_doc_porting_baobab_1autotoc_md19">
<title>GraphAttributes</title>
<para>Some <bold>types</bold> have been renamed and moved to <computeroutput><ref refid="graphics_8h" kindref="compound">graphics.h</ref></computeroutput>:</para>
<para><table rows="4" cols="2"><row>
<entry thead="yes"><para>old name   </para>
</entry><entry thead="yes"><para>new name    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>GraphAttributes::BrushPattern</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>FillPattern</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>GraphAttributes::EdgeArrow</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>EdgeArrow</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>GraphAttributes::EdgeStyle</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>StrokeType</computeroutput>   </para>
</entry></row>
</table>
</para>
<para>The scope of some <bold>attribute bit specifiers</bold> (for the constructor of <computeroutput>GraphAttributes</computeroutput>) has grown such that some other could be removed: <computeroutput>GraphAttributes::nodeStyle</computeroutput> now implies <computeroutput>GraphAttributes::nodeColor</computeroutput> which has hence been removed. In the same way, use <computeroutput>GraphAttributes::edgeStyle</computeroutput> instead of <computeroutput>GraphAttributes::edgeColor</computeroutput>. <computeroutput>GraphAttributes::nodeLevel</computeroutput> has just been removed.</para>
<para>Some <bold>attribute access methods</bold> have been renamed for consistency. The following table shows the changes.</para>
<para><table rows="13" cols="3"><row>
<entry thead="yes"><para>old name   </para>
</entry><entry thead="yes"><para>new name   </para>
</entry><entry thead="yes"><para>remark    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>arrowEdge</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>arrowType</computeroutput>   </para>
</entry><entry thead="no"><para>attribute type has been changed from <computeroutput>GraphAttributes::EdgeArrow</computeroutput> to <computeroutput>EdgeArrow</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>colorEdge</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>strokeColor</computeroutput>   </para>
</entry><entry thead="no"><para>colors are now represented by <computeroutput>Color</computeroutput> (instead of <computeroutput>String</computeroutput>)    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>colorNode</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>fillColor</computeroutput>   </para>
</entry><entry thead="no"><para>colors are now represented by <computeroutput>Color</computeroutput> (instead of <computeroutput>String</computeroutput>)    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>edgeWidth</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>strokeWidth</computeroutput>   </para>
</entry><entry thead="no"><para>attribute type has been changed from <computeroutput>double</computeroutput> to <computeroutput>float</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>labelEdge</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>label</computeroutput>   </para>
</entry><entry thead="no"><para></para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>labelNode</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>label</computeroutput>   </para>
</entry><entry thead="no"><para></para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>lineWidthNode</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>strokeWidth</computeroutput>   </para>
</entry><entry thead="no"><para>attribute type has been changed from <computeroutput>double</computeroutput> to <computeroutput>float</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>nodeLine</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>strokeColor</computeroutput>   </para>
</entry><entry thead="no"><para>colors are now represented by <computeroutput>Color</computeroutput> (instead of <computeroutput>String</computeroutput>)    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>nodePattern</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>fillPattern</computeroutput>   </para>
</entry><entry thead="no"><para>attribute type has been changed from <computeroutput>GraphAttributes::BrushPattern</computeroutput> to <computeroutput>FillPattern</computeroutput>; use <computeroutput>setFillPattern</computeroutput> to change the attribute value    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>shapeNode</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>shape</computeroutput>   </para>
</entry><entry thead="no"><para>attribute type has been changed from <computeroutput>int</computeroutput> to <computeroutput>Shape</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>styleEdge</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>strokeType</computeroutput>   </para>
</entry><entry thead="no"><para>attribute type has been changed from <computeroutput>GraphAttributes::EdgeStyle</computeroutput> to <computeroutput>StrokeType</computeroutput>; use <computeroutput>setStrokeType</computeroutput> to change the attribute value    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>styleNode</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>strokeType</computeroutput>   </para>
</entry><entry thead="no"><para>attribute type has been changed from <computeroutput>GraphAttributes::EdgeStyle</computeroutput> to <computeroutput>StrokeType</computeroutput>; use <computeroutput>setStrokeType</computeroutput> to change the attribute value   </para>
</entry></row>
</table>
</para>
<para>The setter method <computeroutput>directed</computeroutput> has been <bold>renamed</bold> to <computeroutput>setDirected</computeroutput>.</para>
<para>Some types and attributes have been <bold>removed</bold> as they are not used anymore:<itemizedlist>
<listitem><para>types: <computeroutput>GraphAttributes::ImageAlignment</computeroutput>, <computeroutput>GraphAttributes::ImageStyle</computeroutput></para>
</listitem><listitem><para>methods: <computeroutput>imageAlignmentNode</computeroutput>, <computeroutput>imageDrawLineNode</computeroutput>, <computeroutput>imageHeightNode</computeroutput>, <computeroutput>imageStyleNode</computeroutput>, <computeroutput>imageUriNode</computeroutput>, <computeroutput>imageWidthNode</computeroutput></para>
</listitem></itemizedlist>
</para>
<para>Some methods for reading and writing graphs have been <bold>moved</bold> to GraphIO (as static methods, where <computeroutput>GraphAttributes</computeroutput> is the first parameter):</para>
<para><table rows="6" cols="2"><row>
<entry thead="yes"><para>old method   </para>
</entry><entry thead="yes"><para>new method    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>readGML</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>GraphIO::readGML</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>writeGML</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>GraphIO::writeGML</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>readRudy</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>GraphIO::readRudy</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>writeRudy</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>GraphIO::writeRudy</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>writeSVG</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>GraphIO::drawSVG</computeroutput>   </para>
</entry></row>
</table>
</para>
</sect3>
<sect3 id="md_doc_porting_baobab_1autotoc_md20">
<title>GraphCopy</title>
<para>Use <computeroutput>delNode()</computeroutput> and <computeroutput>delEdge()</computeroutput> (as you would do in a <computeroutput>Graph</computeroutput>) instead of <computeroutput>delCopy()</computeroutput>.</para>
</sect3>
<sect3 id="md_doc_porting_baobab_1autotoc_md21">
<title>SteinLibParser</title>
<para>This class has been <bold>removed</bold>. The SteinLib file reader (<computeroutput>SteinLibParser::readSteinLibInstance</computeroutput>) is now in <computeroutput>GraphIO</computeroutput> (<computeroutput>GraphIO::readSTP</computeroutput>).</para>
</sect3>
<sect3 id="md_doc_porting_baobab_1autotoc_md22">
<title>String</title>
<para>This class has been <bold>removed</bold>; use <computeroutput>std::string</computeroutput> instead.</para>
<para>Users of the <computeroutput>sprintf</computeroutput> methods should replace code like <programlisting><codeline><highlight class="normal">{c++}</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/>GA.label(v).sprintf(&quot;Node<sp/>%d&quot;,<sp/>v-&gt;index()<sp/>+<sp/>1);</highlight></codeline>
</programlisting> by code like <programlisting><codeline><highlight class="normal">{c++}</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/>GA.label(v)<sp/>=<sp/>&quot;Node<sp/>&quot;<sp/>+<sp/>to_string(v-&gt;index()<sp/>+<sp/>1);</highlight></codeline>
</programlisting> For more sophisticated formatting, use a stringstream.</para>
</sect3>
<sect3 id="md_doc_porting_baobab_1autotoc_md23">
<title>PlanarizationLayout</title>
<para>Different modules, use now <computeroutput>SubgraphPlanarizer</computeroutput></para>
</sect3>
<sect3 id="md_doc_porting_baobab_1autotoc_md24">
<title>TwoLayerCrossMin</title>
<para>Has been renamed to <computeroutput>LayerByLayerSweep</computeroutput>.</para>
</sect3>
</sect2>
<sect2 id="md_doc_porting_baobab_1autotoc_md25">
<title>Methods</title>
<para>Some methods that are common in some classes are changed/renamed/moved.</para>
<sect3 id="md_doc_porting_baobab_1autotoc_md26">
<title>read* and write*</title>
<para>These methods are usually moved to <computeroutput>GraphIO</computeroutput>.</para>
</sect3>
<sect3 id="md_doc_porting_baobab_1autotoc_md27">
<title>delCopy()</title>
<para>Use <computeroutput>delEdge()</computeroutput> or <computeroutput>delNode()</computeroutput> instead.</para>
</sect3>
<sect3 id="md_doc_porting_baobab_1autotoc_md28">
<title>getGraph()</title>
<para>If the graph is read-only, use <computeroutput>constGraph()</computeroutput>.</para>
</sect3>
</sect2>
<sect2 id="md_doc_porting_baobab_1autotoc_md29">
<title>Files</title>
<sect3 id="md_doc_porting_baobab_1autotoc_md30">
<title>simple_graph_load.h</title>
<para>This file has been completely <bold>removed</bold>. All its functions are now static methods in the class <computeroutput>GraphIO</computeroutput>, though with a different name. The following table explains the name changes.</para>
<para><table rows="12" cols="2"><row>
<entry thead="yes"><para>old name   </para>
</entry><entry thead="yes"><para>new name    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>loadRomeGraph</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>GraphIO::readRome</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>loadChacoGraph</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>GraphIO::readChaco</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>loadSimpleGraph</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>GraphIO::readPMDissGraph</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>loadYGraph</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>GraphIO::readYGraph</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>loadChallengeGraph</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>GraphIO::readChallengeGraph</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>saveChallengeGraph</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>GraphIO::writeChallengeGraph</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>loadEdgeListSubgraph</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>GraphIO::readEdgeListSubgraph</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>saveEdgeListSubgraph</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>GraphIO::writeEdgeListSubgraph</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>loadBenchHypergraph</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>GraphIO::writeChaco</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>loadPlaHypergraph</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>GraphIO::readPLA</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>loadBenchHypergraph</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>GraphIO::readBENCH</computeroutput>   </para>
</entry></row>
</table>
</para>
<para>For <computeroutput>loadYGraph</computeroutput>, note that the input file is now specified by <computeroutput>istream</computeroutput> or a filename string, instead of a C-style <computeroutput>FILE</computeroutput> handle.</para>
</sect3>
<sect3 id="md_doc_porting_baobab_1autotoc_md31">
<title>SteinLibParser.h</title>
<para>This file has been completely <bold>removed</bold>. The SteinLib file reader (<computeroutput>SteinLibParser::readSteinLibInstance</computeroutput>) is now in <computeroutput>GraphIO</computeroutput> (<computeroutput>GraphIO::readSTP</computeroutput>).</para>
</sect3>
</sect2>
<sect2 id="md_doc_porting_baobab_1autotoc_md32">
<title>General Concepts</title>
<sect3 id="md_doc_porting_baobab_1autotoc_md33">
<title>Colors</title>
<para>Colors are now represented by the class <computeroutput>Color</computeroutput>, and no longer by strings of the form <computeroutput>&quot;#RRGGBB&quot;</computeroutput>. For porting these string representations, <computeroutput>Color</computeroutput> offers you several options:<itemizedlist>
<listitem><para>You can construct a <computeroutput>Color</computeroutput> from its string representation by passing the string in the constructor: <computeroutput>Color color(&quot;ff0000&quot;);</computeroutput></para>
</listitem><listitem><para>You can set a color from the string representation: <computeroutput>Color color; ... color.fromString(&quot;ff0000&quot;);</computeroutput></para>
</listitem><listitem><para>You can decompose the string representation and construct the color from its components: <computeroutput>Color color(0xff,0x00,0x00);</computeroutput></para>
</listitem><listitem><para>You can use named colors from the <computeroutput>Color::Name</computeroutput> enumeration: <computeroutput>Color color(Color::Red);</computeroutput></para>
</listitem></itemizedlist>
</para>
<para>The most readable form is the latter one, so it might be useful to adopt this version where possible; the <computeroutput>Color::Name</computeroutput> enumeration is quite extensive and corresponds to the <ulink url="http://www.w3.org/TR/SVG/types.html#ColorKeywords">SVG color keywords</ulink>.</para>
</sect3>
<sect3 id="md_doc_porting_baobab_1autotoc_md34">
<title>GraphIO</title>
<para>GraphIO handles reading from and writing to several graph formats, saving to SVG vector graphics format, and so on.</para>
<para>Simple usage example: <programlisting><codeline><highlight class="normal"><sp/>{c++}</highlight></codeline>
<codeline><highlight class="normal">include<sp/>&lt;ogdf/fileformats/GraphIO.h&gt;</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">…</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>GraphIO::readRome(G,<sp/>&quot;foo&quot;);</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>…</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>GraphIO::writeGML(G,<sp/>&quot;foo.gml&quot;);</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>…</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>GraphIO::drawSVG(GA,<sp/>&quot;foo.svg&quot;);</highlight></codeline>
<codeline><highlight class="normal">…</highlight></codeline>
</programlisting></para>
</sect3>
</sect2>
<sect2 id="md_doc_porting_baobab_1autotoc_md35">
<title>Third-party Libraries</title>
<para>Some code uses LP solvers or SAT solvers. To keep the third-party dependencies of OGDF small and ease the use of that solvers, such solvers are now included or integrated into OGDF.</para>
<sect3 id="md_doc_porting_baobab_1autotoc_md36">
<title>Abacus</title>
<para>Abacus 3.2 is now integrated into OGDF, in its own namespace named <computeroutput>abacus</computeroutput>.</para>
<sect4 id="md_doc_porting_baobab_1autotoc_md37">
<title>Abacus Classes</title>
<para>All classes like <computeroutput>ABA_FOOBAR</computeroutput> have been renamed such that the <computeroutput>ABA_</computeroutput> prefix is removed and the suffix is camel-cased, like <computeroutput>FooBar</computeroutput>. Examples are <computeroutput>Master</computeroutput>, <computeroutput>VarType</computeroutput>, <computeroutput>Variable</computeroutput>, <computeroutput>Sub</computeroutput>, <computeroutput>CSense</computeroutput>, <computeroutput>FSVarStat</computeroutput>.</para>
<para>There are exceptions and some classes have also been removed. See the following list:</para>
<para><table rows="4" cols="2"><row>
<entry thead="yes"><para>old name   </para>
</entry><entry thead="yes"><para>use instead    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>ABA_BUFFER</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>ArrayBuffer</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>ABA_CPUTIMER</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>StopwatchCPU</computeroutput>    </para>
</entry></row>
<row>
<entry thead="no"><para><computeroutput>ABA_STRING</computeroutput>   </para>
</entry><entry thead="no"><para><computeroutput>string</computeroutput>   </para>
</entry></row>
</table>
</para>
</sect4>
<sect4 id="md_doc_porting_baobab_1autotoc_md38">
<title>Missing Header Files</title>
<para>Because Abacus&apos; own timers are replaced by OGDF timers, the file <computeroutput>abacus/timer.h</computeroutput> has been removed. Instead include <computeroutput><ref refid="_stopwatch_8h" kindref="compound">ogdf/basic/Stopwatch.h</ref></computeroutput> and use the <computeroutput>StopwatchWallClock</computeroutput> or <computeroutput>StopwatchCPU</computeroutput> classes. Note that they must not be initialized with Abacus objects.</para>
</sect4>
<sect4 id="md_doc_porting_baobab_1autotoc_md39">
<title>Const Correctness</title>
<para>Abacus is not fully const correct. For the OGDF integration, missing <computeroutput>const</computeroutput> declarations have been inserted. However, when using old Abacus code (written against the original Abacus) this may lead to non-compiling code when you inherit from Abacus classes with virtual methods that were non-const and are now const.</para>
<para>For example, virtual <computeroutput>Constraint</computeroutput> methods like <computeroutput>print</computeroutput> and <computeroutput>coeff</computeroutput> are all declared const now. If you define non-const <computeroutput>print</computeroutput> and <computeroutput>coeff</computeroutput> in your own constraints, the const <computeroutput>print</computeroutput> and <computeroutput>coeff</computeroutput> version are still undefined. C++ compilers like <computeroutput>g++</computeroutput> may hence display errors like &quot;`cannot allocate an object of abstract type ‘YourConstraintType’`&quot;. Adding <computeroutput>const</computeroutput> keywords in the right position(s) should solve the problems.</para>
</sect4>
</sect3>
<sect3 id="md_doc_porting_baobab_1autotoc_md40">
<title>COIN-OR</title>
<para>COIN-OR (Clp 1.14.7 and Symphony 5.4.5) is included in the OGDF source tree. However, it is not linked to the OGDF binary.</para>
<para>Only minor changes have been made to the COIN-OR source which are documented in <computeroutput>src/coin/Readme.txt</computeroutput>. </para>
</sect3>
</sect2>
</sect1>
    </detaileddescription>
    <location file="doc/porting/baobab.md"/>
  </compounddef>
</doxygen>
